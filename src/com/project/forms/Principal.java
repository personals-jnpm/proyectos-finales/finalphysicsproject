package com.project.forms;

import com.project.design.paneles.FrmAbout;
import com.project.design.paneles.FrmCalculator;
import java.awt.Color;
import java.awt.MouseInfo;
import java.awt.Point;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import com.project.design.paneles.FrmHome;
import com.project.design.paneles.FrmSimulator;
import com.project.utilities.OpenWindow;
import java.beans.PropertyVetoException;
import javax.swing.ImageIcon;

public class Principal extends javax.swing.JFrame {

    int x, y;

    public Principal() {
        initComponents();
        init();
    }

    private void init() {
        try {
            this.setExtendedState(MAXIMIZED_BOTH);
            this.setLocationRelativeTo(this);
            this.menuHome.setSelected(true);
            OpenWindow.AbrirVentana(FrmHome.class, desktopPane, true);
            setIconImage(new ImageIcon(Objects.requireNonNull(getClass().getResource("/com/project/images/ICON.png"))).getImage());
        } catch (PropertyVetoException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnlnMain = new javax.swing.JPanel();
        pnlMenu = new javax.swing.JPanel();
        menuHome = new com.project.design.object.RSButtonMetro();
        panelSubtitle = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        menuSimulador = new com.project.design.object.RSButtonMetro();
        menuCalculator = new com.project.design.object.RSButtonMetro();
        menuAbout = new com.project.design.object.RSButtonMetro();
        pnlHeader = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        pnlCentro = new javax.swing.JPanel();
        scrollPane1 = new javax.swing.JScrollPane();
        desktopPane = new javax.swing.JDesktopPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Movimiento Parabólico");
        setResizable(false);

        pnlnMain.setBackground(new java.awt.Color(255, 255, 255));
        pnlnMain.setLayout(new java.awt.GridBagLayout());

        pnlMenu.setBackground(new java.awt.Color(239, 238, 244));
        pnlMenu.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 5, 0, 0, new java.awt.Color(239, 238, 244)));

        menuHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/project/images/homex32.png"))); // NOI18N
        menuHome.setText("Inicio");
        menuHome.setColorHover(new java.awt.Color(204, 204, 204));
        menuHome.setColorNormal(new java.awt.Color(204, 204, 204));
        menuHome.setColorPressed(new java.awt.Color(204, 204, 204));
        menuHome.setColorTextHover(new java.awt.Color(128, 128, 131));
        menuHome.setColorTextNormal(new java.awt.Color(128, 128, 131));
        menuHome.setColorTextPressed(new java.awt.Color(128, 128, 131));
        menuHome.setFocusable(false);
        menuHome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        menuHome.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        menuHome.setIconTextGap(25);
        menuHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                menuHomeMousePressed(evt);
            }
        });
        menuHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuHomeActionPerformed(evt);
            }
        });

        panelSubtitle.setBackground(new java.awt.Color(239, 238, 244));
        panelSubtitle.setForeground(new java.awt.Color(38, 86, 186));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 102));
        jLabel1.setText("MENÚ");

        javax.swing.GroupLayout panelSubtitleLayout = new javax.swing.GroupLayout(panelSubtitle);
        panelSubtitle.setLayout(panelSubtitleLayout);
        panelSubtitleLayout.setHorizontalGroup(
                panelSubtitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panelSubtitleLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addContainerGap(111, Short.MAX_VALUE))
        );
        panelSubtitleLayout.setVerticalGroup(
                panelSubtitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSubtitleLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1)
                                .addContainerGap())
        );

        menuSimulador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/project/images/line-chartx32.png"))); // NOI18N
        menuSimulador.setText("Simulador");
        menuSimulador.setColorHover(new java.awt.Color(204, 204, 204));
        menuSimulador.setColorNormal(new java.awt.Color(239, 238, 244));
        menuSimulador.setColorPressed(new java.awt.Color(204, 204, 204));
        menuSimulador.setColorTextHover(new java.awt.Color(128, 128, 131));
        menuSimulador.setColorTextNormal(new java.awt.Color(128, 128, 131));
        menuSimulador.setColorTextPressed(new java.awt.Color(128, 128, 131));
        menuSimulador.setFocusable(false);
        menuSimulador.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        menuSimulador.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        menuSimulador.setIconTextGap(19);
        menuSimulador.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                menuSimuladorMousePressed(evt);
            }
        });
        menuSimulador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSimuladorActionPerformed(evt);
            }
        });

        menuCalculator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/project/images/calculatorx32.png"))); // NOI18N
        menuCalculator.setText("Calculadora");
        menuCalculator.setColorHover(new java.awt.Color(204, 204, 204));
        menuCalculator.setColorNormal(new java.awt.Color(239, 238, 244));
        menuCalculator.setColorPressed(new java.awt.Color(204, 204, 204));
        menuCalculator.setColorTextHover(new java.awt.Color(128, 128, 131));
        menuCalculator.setColorTextNormal(new java.awt.Color(128, 128, 131));
        menuCalculator.setColorTextPressed(new java.awt.Color(128, 128, 131));
        menuCalculator.setFocusable(false);
        menuCalculator.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        menuCalculator.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        menuCalculator.setIconTextGap(25);
        menuCalculator.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                menuCalculatorMousePressed(evt);
            }
        });
        menuCalculator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCalculatorActionPerformed(evt);
            }
        });

        menuAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/project/images/infox32.png"))); // NOI18N
        menuAbout.setText("Acerca de");
        menuAbout.setColorHover(new java.awt.Color(204, 204, 204));
        menuAbout.setColorNormal(new java.awt.Color(239, 238, 244));
        menuAbout.setColorPressed(new java.awt.Color(204, 204, 204));
        menuAbout.setColorTextHover(new java.awt.Color(128, 128, 131));
        menuAbout.setColorTextNormal(new java.awt.Color(128, 128, 131));
        menuAbout.setColorTextPressed(new java.awt.Color(128, 128, 131));
        menuAbout.setFocusable(false);
        menuAbout.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        menuAbout.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        menuAbout.setIconTextGap(19);
        menuAbout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                menuAboutMousePressed(evt);
            }
        });
        menuAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAboutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlMenuLayout = new javax.swing.GroupLayout(pnlMenu);
        pnlMenu.setLayout(pnlMenuLayout);
        pnlMenuLayout.setHorizontalGroup(
                pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlMenuLayout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(menuCalculator, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                                        .addComponent(menuSimulador, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                                        .addComponent(menuAbout, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                                        .addComponent(panelSubtitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(menuHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlMenuLayout.setVerticalGroup(
                pnlMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlMenuLayout.createSequentialGroup()
                                .addComponent(panelSubtitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(menuHome, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(menuCalculator, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(menuSimulador, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(menuAbout, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(399, 399, 399))
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 8.3;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 1);
        pnlnMain.add(pnlMenu, gridBagConstraints);

        pnlHeader.setBackground(new java.awt.Color(0, 153, 153));
        pnlHeader.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        pnlHeader.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                pnlHeaderMouseDragged(evt);
            }
        });
        pnlHeader.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pnlHeaderMousePressed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/project/images/icon-atom.png"))); // NOI18N
        jButton1.setBorder(null);
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("MOVIMIENTO PARABÓLICO");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/project/images/logo-ustax32.png"))); // NOI18N

        javax.swing.GroupLayout pnlHeaderLayout = new javax.swing.GroupLayout(pnlHeader);
        pnlHeader.setLayout(pnlHeaderLayout);
        pnlHeaderLayout.setHorizontalGroup(
                pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlHeaderLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 825, Short.MAX_VALUE)
                                .addComponent(jLabel4)
                                .addContainerGap())
        );
        pnlHeaderLayout.setVerticalGroup(
                pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlHeaderLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.weightx = 0.2;
        pnlnMain.add(pnlHeader, gridBagConstraints);

        pnlCentro.setBackground(new java.awt.Color(255, 255, 255));

        scrollPane1.setBorder(null);

        desktopPane.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout desktopPaneLayout = new javax.swing.GroupLayout(desktopPane);
        desktopPane.setLayout(desktopPaneLayout);
        desktopPaneLayout.setHorizontalGroup(
                desktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 899, Short.MAX_VALUE)
        );
        desktopPaneLayout.setVerticalGroup(
                desktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 660, Short.MAX_VALUE)
        );

        scrollPane1.setViewportView(desktopPane);

        javax.swing.GroupLayout pnlCentroLayout = new javax.swing.GroupLayout(pnlCentro);
        pnlCentro.setLayout(pnlCentroLayout);
        pnlCentroLayout.setHorizontalGroup(
                pnlCentroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(scrollPane1)
        );
        pnlCentroLayout.setVerticalGroup(
                pnlCentroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(scrollPane1)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.4;
        gridBagConstraints.weighty = 0.1;
        pnlnMain.add(pnlCentro, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pnlnMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pnlnMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//        int posicion = pnlMenu.getX();
//        if (posicion > -1) {
//            Animacion.Animacion.mover_izquierda(0, -264, 2, 2, pnlMenu);
//        } else {
//            Animacion.Animacion.mover_derecha(-264, 0, 2, 2, pnlMenu);
//        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void pnlHeaderMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlHeaderMousePressed
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_pnlHeaderMousePressed

    private void pnlHeaderMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlHeaderMouseDragged
        Point mueve = MouseInfo.getPointerInfo().getLocation();
        this.setLocation(mueve.x - x, mueve.y - y);
    }//GEN-LAST:event_pnlHeaderMouseDragged

    private void menuAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAboutActionPerformed
        paint();
        if (this.menuAbout.isSelected()) {
            this.menuAbout.setColorNormal(new Color(204, 204, 204));
            this.menuAbout.setColorHover(new Color(204, 204, 204));
            this.menuAbout.setColorPressed(new Color(204, 204, 204));
        }
        try {
            OpenWindow.AbrirVentana(FrmAbout.class, desktopPane, true);
        } catch (PropertyVetoException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menuAboutActionPerformed

    private void menuAboutMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuAboutMousePressed
        select();
        menuAbout.setSelected(true);
    }//GEN-LAST:event_menuAboutMousePressed

    private void menuCalculatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCalculatorActionPerformed
        paint();
        if (this.menuCalculator.isSelected()) {
            this.menuCalculator.setColorNormal(new Color(204, 204, 204));
            this.menuCalculator.setColorHover(new Color(204, 204, 204));
            this.menuCalculator.setColorPressed(new Color(204, 204, 204));
        }
        try {
            OpenWindow.AbrirVentana(FrmCalculator.class, desktopPane, true);
        } catch (PropertyVetoException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menuCalculatorActionPerformed

    private void menuCalculatorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuCalculatorMousePressed
        select();
        menuCalculator.setSelected(true);
    }//GEN-LAST:event_menuCalculatorMousePressed

    private void menuSimuladorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSimuladorActionPerformed
        paint();
        if (this.menuSimulador.isSelected()) {
            this.menuSimulador.setColorNormal(new Color(204, 204, 204));
            this.menuSimulador.setColorHover(new Color(204, 204, 204));
            this.menuSimulador.setColorPressed(new Color(204, 204, 204));
        }
        try {
            OpenWindow.AbrirVentana(FrmSimulator.class, desktopPane, true);
        } catch (PropertyVetoException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menuSimuladorActionPerformed

    private void menuSimuladorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuSimuladorMousePressed
        select();
        menuSimulador.setSelected(true);
    }//GEN-LAST:event_menuSimuladorMousePressed

    private void menuHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuHomeActionPerformed
        paint();
        if (this.menuHome.isSelected()) {
            this.menuHome.setColorNormal(new Color(204, 204, 204));
            this.menuHome.setColorHover(new Color(204, 204, 204));
            this.menuHome.setColorPressed(new Color(204, 204, 204));
        }
        try {
            OpenWindow.AbrirVentana(FrmHome.class, desktopPane, true);
        } catch (PropertyVetoException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menuHomeActionPerformed

    private void menuHomeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuHomeMousePressed
        select();
        menuHome.setSelected(true);
    }//GEN-LAST:event_menuHomeMousePressed

    private void select() {
        this.menuHome.setSelected(false);
        this.menuCalculator.setSelected(false);
        this.menuSimulador.setSelected(false);
        this.menuAbout.setSelected(false);
    }

    private void paint() {
        this.menuHome.setColorNormal(new Color(239, 238, 244));
        this.menuHome.setColorHover(new Color(204, 204, 204));
        this.menuHome.setColorPressed(new Color(204, 204, 204));

        this.menuCalculator.setColorNormal(new Color(239, 238, 244));
        this.menuCalculator.setColorHover(new Color(204, 204, 204));
        this.menuCalculator.setColorPressed(new Color(204, 204, 204));

        this.menuSimulador.setColorNormal(new Color(239, 238, 244));
        this.menuSimulador.setColorHover(new Color(204, 204, 204));
        this.menuSimulador.setColorPressed(new Color(204, 204, 204));

        this.menuAbout.setColorNormal(new Color(239, 238, 244));
        this.menuAbout.setColorHover(new Color(204, 204, 204));
        this.menuAbout.setColorPressed(new Color(204, 204, 204));

    }

    public static void main(String[] args) {

        java.awt.EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                new Principal().setVisible(true);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private com.project.design.object.RSButtonMetro menuAbout;
    private com.project.design.object.RSButtonMetro menuCalculator;
    private com.project.design.object.RSButtonMetro menuHome;
    private com.project.design.object.RSButtonMetro menuSimulador;
    private javax.swing.JPanel panelSubtitle;
    private javax.swing.JPanel pnlCentro;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JPanel pnlMenu;
    private javax.swing.JPanel pnlnMain;
    private javax.swing.JScrollPane scrollPane1;
    // End of variables declaration//GEN-END:variables
}
