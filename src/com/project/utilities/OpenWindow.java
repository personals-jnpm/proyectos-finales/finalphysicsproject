package com.project.utilities;

import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyVetoException;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.plaf.basic.BasicInternalFrameUI;

public class OpenWindow {

    //Método Para abrir una InternalFrame en un Escritorio
    public static void AbrirVentana(Class myFrame, JDesktopPane escritorio, boolean forzar) throws PropertyVetoException, InstantiationException, IllegalAccessException {
        int cantidad;
        Component[] arreglo = escritorio.getAllFrames();
        cantidad = arreglo.length;
        if (cantidad > 0) {
            if (forzar) {
                escritorio.removeAll();
                configuracionVentana(myFrame, escritorio);
            } else {
                for (Component objeto : arreglo) {
                    if (myFrame.isInstance(objeto)) {
                        JOptionPane.showMessageDialog(escritorio, "Ventana abierta");
                    } else {
                        JOptionPane.showMessageDialog(escritorio, "No se puede");
                    }
                }
            }
        } else {
            configuracionVentana(myFrame, escritorio);
        }
    }

    //Método para preparar la ventana que será abierta
    private static void configuracionVentana(Class myFrame, JDesktopPane escritorio) throws PropertyVetoException, InstantiationException, IllegalAccessException {
        JInternalFrame ventana = (JInternalFrame) myFrame.newInstance();
        escritorio.add(ventana);
        ventana.setVisible(true);
        ventana.setMaximum(true);
        BasicInternalFrameUI ui = (BasicInternalFrameUI) ventana.getUI();
        Container esquinaIzquierda = (Container) ui.getNorthPane();
        esquinaIzquierda.remove(0);
        esquinaIzquierda.validate();
        esquinaIzquierda.repaint();
        ui.setNorthPane(null);
    }

}
