package com.project.utilities;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Validate {

    public static boolean isValidDouble(String data) {
        String cadena;
        cadena = "[0-9]+(\\.[0-9]*)?";
        return data.matches(cadena);
    }

    public static boolean isValidInteger(String data) {
        String cadena;
        cadena = "[0-9]*";
        return data.matches(cadena);
    }


    private static double convertAngle(double angle) {
        int turn;
        double newAngle;

        turn = (int) (angle / 360);
        newAngle = angle - (360 * turn);

        if (newAngle < 0) {
            newAngle = 360 + newAngle;
        }
        return newAngle;
    }

    public static boolean validateAngle(double angle) {
        double newAngle;

        newAngle = convertAngle(angle);

        return newAngle > 0 && newAngle < 90;
    }

    public static double round(int digit, double number) {
        BigDecimal dec;
        double newNumber;

        try {
            dec = new BigDecimal(number);
        } catch (Exception e) {
            return (Math.random() * 1000000) + 1;
        }
        newNumber = dec.setScale(digit, RoundingMode.HALF_UP).doubleValue();
        return newNumber;
    }

}
