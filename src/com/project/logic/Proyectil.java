package com.project.logic;

import com.project.utilities.Validate;

public class Proyectil {

    private static final double GRAVITY = 9.8;
    private static final int DIGITS = 3;

    public Proyectil() {
    }

    public double getAngleVA(double initialVelocity, double maximumReach) {
        double result;
        result = Math.toDegrees(Math.asin(maximumReach * GRAVITY / Math.pow(initialVelocity, 2)));

        return Validate.round(DIGITS, result / 2);
    }

    public double getAngleVH(double initialVelocity, double height) {
        double result;
        result = Math.toDegrees(Math.asin(Math.sqrt(height * 2 * GRAVITY / Math.pow(initialVelocity, 2))));

        return Validate.round(DIGITS, result);
    }

    public double getAngleVT(double initialVelocity, double time) {
        double result;
        result = Math.toDegrees(Math.asin((time * GRAVITY) / (2 * initialVelocity)));

        return Validate.round(DIGITS, result);
    }

    public double getAngleVXY(double vx, double vy) {
        double result;
        result = Math.toDegrees(Math.asin(vy / Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2))));

        return Validate.round(DIGITS, result);
    }

    public double getVelocityVXY(double vx, double vy) {
        double result;
        result = Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));

        return Validate.round(DIGITS, result);
    }

    public double getVelocityAD(double maximumReach, double angle) {
        double result;
        result = Math.sqrt(maximumReach * GRAVITY / Math.sin(Math.toRadians(2 * angle)));

        return Validate.round(DIGITS, result);
    }

    public double getVelocityHD(double height, double angle) {
        double result;
        result = Math.sqrt(height * 2 * GRAVITY / Math.pow(Math.sin(Math.toRadians(angle)), 2));

        return Validate.round(DIGITS, result);
    }

    public double getVelocityTD(double time, double angle) {
        double result;
        result = (time * GRAVITY) / (2 * Math.sin(Math.toRadians(angle)));

        return Validate.round(DIGITS, result);
    }

}
