package com.project.logic;

import com.project.utilities.Validate;

public class LanzamientoP {

    private static final double GRAVITY = 9.8;
    private static final int DIGITS = 3;
    private double initialVelocity;
    private double angle;

    public LanzamientoP() {
    }

    public LanzamientoP(double initialVelocity, double angle) {
        this.initialVelocity = initialVelocity;
        this.angle = angle;
    }

    public double getInitialVelocity() {
        return initialVelocity;
    }

    public void setInitialVelocity(double initialVelocity) {
        this.initialVelocity = initialVelocity;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getAlcanceM() {
        return Validate.round(DIGITS, getAlcanceMr());
    }

    public double getAlcanceMr() {
        return Math.pow(initialVelocity, 2) * Math.sin(Math.toRadians(angle * 2)) / GRAVITY;
    }

    public double getAlturaM() {
        double result;
        result = (Math.pow(initialVelocity, 2) * Math.pow(Math.sin(Math.toRadians(angle)), 2)) / (2 * GRAVITY);

        return Validate.round(DIGITS, result);
    }

    public double getTimef() {
        double result;
        result = 2 * (initialVelocity * Math.sin(Math.toRadians(angle))) / GRAVITY;

        return Validate.round(DIGITS, result);
    }

    public double getAlcance(double time) {
        double result;
        result = initialVelocity * Math.cos(Math.toRadians(angle)) * time;

        return Validate.round(DIGITS, result);
    }

    public double getAltura(double time) {
        double result;
        result = (initialVelocity * Math.sin(Math.toRadians(angle)) * time) - (0.5 * GRAVITY * Math.pow(time, 2));

        return Validate.round(DIGITS, result);
    }

    private double getVox() {
        return initialVelocity * Math.cos(Math.toRadians(angle));
    }

    public double getVoxI() {
        return Validate.round(DIGITS, getVox());
    }

    public double getVoyI() {
        return Validate.round(DIGITS, getVoy());
    }

    private double getVoy() {
        return initialVelocity * Math.sin(Math.toRadians(angle));
    }

    public double getVx() {
        return getVoxI();
    }

    public double getVy(double time) {
        double result;
        result = (initialVelocity * Math.sin(Math.toRadians(angle))) - (GRAVITY * time);

        return Validate.round(DIGITS, result);
    }

    public double getV(double time) {
        double vx = getVx();
        double vy = getVy(time);

        return Validate.round(DIGITS, Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2)));
    }

    public double getTimeH(double height, boolean s) {
        double result, time;
        System.out.println(initialVelocity * Math.sin(Math.toRadians(angle)));
        result = Math.pow(initialVelocity * Math.sin(Math.toRadians(angle)), 2) - (4 * GRAVITY * -0.5 * -height);
        if (result < 0) {
            return -1;
        }
        if (s) {
            time = (-initialVelocity * Math.sin(Math.toRadians(angle)) + Math.sqrt(result)) / (2 * GRAVITY * -0.5);
        } else {
            time = (-initialVelocity * Math.sin(Math.toRadians(angle)) - Math.sqrt(result)) / (2 * GRAVITY * -0.5);
        }
        return time;
    }

    public double getTimeA(double alcance) {
        if (alcance > getAlcanceM()) return -1;

        return alcance / (initialVelocity * Math.cos(Math.toRadians(angle)));
    }

    public double getTimeVy(double velocidad) {
        double result = velocidad - (initialVelocity * Math.sin(Math.toRadians(angle)));
        return result / -GRAVITY;
    }

    public double getTimeV(double velocidad) {
        double vx = initialVelocity * Math.cos(Math.toRadians(angle));
        double time = Math.sqrt(Math.pow(velocidad, 2) - Math.pow(vx, 2));

        return getTimeVy(time);
    }

    public String getEcuacion() {
        double b = Validate.round(4, getVoy() / getVox());
        double a = Validate.round(4, -(GRAVITY / 2) / (Math.pow(getVox(), 2)));

        return a + "x" + "\u00B2" + " + " + b + "x + " + 0;
    }

    public double[] getEcuacion2() {
        double[] numbers = new double[4];
        numbers[0] = -(GRAVITY / 2) / (Math.pow(getVox(), 2));
        numbers[1] = getVoy() / getVox();
        numbers[2] = 0;
        numbers[3] = 0;

        return numbers;
    }

}
