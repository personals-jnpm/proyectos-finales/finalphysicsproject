package com.project.design.paneles;

import com.project.logic.LanzamientoP;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serial;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class FrmSimulator extends javax.swing.JInternalFrame
        implements ActionListener, ChangeListener {

    @Serial
    private static final long serialVersionUID = 1L;

    private ParabolicShotPanel parabolicShotPanel;

    private int initialAngle;

    private int initialVelocity;

    private int initialHeight;

    int MAX_HEIGHT = 300;

    Timer simulationTimer;

    double currentInstant;

    final double instantsTimeDifference = 0.1;

    final double gravityAcceleration = 9.8;

    int maximumHeightReached;

    boolean trajectoryFinished;

    public FrmSimulator() {
        initComponents();
        initSimulator(800, 500);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        JPanelGraphic = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        textInitialVelocity = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textAngle = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textInitialHeight = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        simTime = new javax.swing.JLabel();
        simulationDistance = new javax.swing.JLabel();
        simulationVelocity = new javax.swing.JLabel();
        simulationVX = new javax.swing.JLabel();
        simulationVY = new javax.swing.JLabel();
        simulationH = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(null);
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setForeground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setFont(new java.awt.Font("Roboto", 1, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 102));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("SIMULADOR");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
                                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(0, 12, Short.MAX_VALUE))
        );

        JPanelGraphic.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout JPanelGraphicLayout = new javax.swing.GroupLayout(JPanelGraphic);
        JPanelGraphic.setLayout(JPanelGraphicLayout);
        JPanelGraphicLayout.setHorizontalGroup(
                JPanelGraphicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 814, Short.MAX_VALUE)
        );
        JPanelGraphicLayout.setVerticalGroup(
                JPanelGraphicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 405, Short.MAX_VALUE)
        );

        jLabel1.setText("Velocidad inicial:");

        jLabel2.setText("Ángulo:");

        jLabel3.setText("Altura inicial:");

        jLabel4.setText("Tiempo:");

        jLabel5.setText("Distancia:");

        jLabel6.setText("Velocidad:");

        jLabel8.setText("Velocidad en x:");

        jLabel9.setText("Velocidad en y:");

        jLabel10.setText("Altura máxima:");

        jButton1.setText("Iniciar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(199, 199, 199)
                                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(51, 51, 51)
                                                .addComponent(JPanelGraphic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(49, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(textInitialVelocity, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(textInitialHeight, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(textAngle, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(53, 53, 53)
                                                .addComponent(jButton1)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel6)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(simulationVelocity, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel4)
                                                        .addComponent(jLabel5))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(simTime, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(simulationDistance, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(36, 36, 36)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel9)
                                                        .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(simulationVY, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(simulationH, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel8)
                                                .addGap(18, 18, 18)
                                                .addComponent(simulationVX, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(84, 84, 84))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JPanelGraphic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(41, 41, 41)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(textInitialVelocity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel8)
                                        .addComponent(simTime)
                                        .addComponent(simulationVX))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(textAngle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel9)
                                        .addComponent(simulationDistance)
                                        .addComponent(simulationVY)
                                        .addComponent(jButton1))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(textInitialHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel6)
                                        .addComponent(jLabel10)
                                        .addComponent(simulationVelocity)
                                        .addComponent(simulationH))
                                .addContainerGap(29, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            initialVelocity = Integer.parseInt(textInitialVelocity.getText());
            initialAngle = Integer.parseInt(textAngle.getText());
            initialHeight = Integer.parseInt(textInitialHeight.getText());

            validateFields();

            startSimulation();
            clearInputs();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Los valores deben ser numéricos");
            clearInputs();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            clearInputs();
        }

    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel JPanelGraphic;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel simTime;
    private javax.swing.JLabel simulationDistance;
    private javax.swing.JLabel simulationH;
    private javax.swing.JLabel simulationVX;
    private javax.swing.JLabel simulationVY;
    private javax.swing.JLabel simulationVelocity;
    private javax.swing.JTextField textAngle;
    private javax.swing.JTextField textInitialHeight;
    private javax.swing.JTextField textInitialVelocity;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent ae) {

    }

    @Override
    public void stateChanged(ChangeEvent ce) {

    }

    class TimerListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            calculateNextTrajectoryPoint();
        }
    }

    public void initSimulator(int width, int height) {

        final int TIMER_DELAY = 20;
        simulationTimer = new Timer(TIMER_DELAY, new TimerListener());
        final int PARABOLIC_SHOT_PANEL_HEIGHT = (int) (height * 0.8);

        parabolicShotPanel = new ParabolicShotPanel(width,
                PARABOLIC_SHOT_PANEL_HEIGHT, initialAngle, initialHeight);

        JPanelGraphic.add(parabolicShotPanel);
    }

    public void clearInputs() {
        textInitialVelocity.setText("");
        textAngle.setText("");
        textInitialHeight.setText("");
    }

    public void validateFields() throws Exception {
        if (initialVelocity < 0 || initialVelocity > 101) {
            throw new Exception("El valor de la velocidad debe estar entre 0 m/s y 100 m/s");
        } else if (initialHeight < 0 || initialHeight > 800) {
            throw new Exception("El valor de la altura inicial debe estar entre 0 m y 800 m");
        } else if (initialAngle < 5 || initialAngle >= 90) {
            throw new Exception("El valor del ángulo de disparo debe estar entre 5° y 89°");
        }
    }

    public ParabolicShotPanel getParabolicShotPanel() {
        return parabolicShotPanel;
    }

    private void calculateNextTrajectoryPoint() {
        double xVelocity = initialVelocity * Math.cos(Math.toRadians(initialAngle));
        double yVelocity = initialVelocity * Math.sin(Math.toRadians(initialAngle))
                - gravityAcceleration * currentInstant;

        double velocity = Math.sqrt(Math.pow(xVelocity, 2) + Math.pow(yVelocity, 2));

        int xPosition = (int) (xVelocity * currentInstant);
        int yPosition = (int) (initialHeight
                + (initialVelocity * Math.sin(Math.toRadians(initialAngle))
                * currentInstant) - (0.5 * gravityAcceleration
                * Math.pow(currentInstant, 2)));

        if (maximumHeightReached < yPosition) {
            maximumHeightReached = yPosition;
        }
        if (maximumHeightReached > MAX_HEIGHT) {
            MAX_HEIGHT = maximumHeightReached;
        }

        if (yPosition < 0 && currentInstant > 0.0) {
            trajectoryFinished = true;
            stopSimulation();
        } else {
            currentInstant += instantsTimeDifference;
            parabolicShotPanel.addPoint(new Point(xPosition, yPosition));

            simTime.setText(new DecimalFormat("#.00").format(currentInstant) + " s");
            simulationDistance.setText(new DecimalFormat("#.00").format(xPosition) + " m");
            simulationH.setText("" + new DecimalFormat("#.00").format(maximumHeightReached) + " m");
            simulationVelocity.setText("" + new DecimalFormat("#.00").format(velocity) + " m/s");
            simulationVX.setText("" + new DecimalFormat("#.00").format(xVelocity) + " m/s");
            simulationVY.setText("" + new DecimalFormat("#.00").format(yVelocity) + " m/s");
        }
    }

    private void newProjectile() {
        parabolicShotPanel.addProjectile();
        currentInstant = 0.0;
        trajectoryFinished = false;
        maximumHeightReached = initialHeight;
    }

    public void startSimulation() {
        newProjectile();
        simulationTimer.start();
    }

    private void stopSimulation() {
        simulationTimer.stop();
        LanzamientoP lanzamiento = new LanzamientoP();
        lanzamiento.setInitialVelocity(initialVelocity);
        lanzamiento.setAngle(initialAngle);
        simTime.setText(new DecimalFormat("#.00").format(lanzamiento.getTimef()) + " s");
        simulationDistance.setText(new DecimalFormat("#.00").format(lanzamiento.getAlcanceM()) + " m");
    }

    private void restartSimulation() {
        if (!trajectoryFinished) {
            simulationTimer.start();
        }
    }
}
