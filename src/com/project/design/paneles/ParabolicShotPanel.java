
package com.project.design.paneles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.io.Serial;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class ParabolicShotPanel extends JPanel {

    @Serial
    private static final long serialVersionUID = 1L;

    private int anguloDisparo;
    private int distanciaMaxima;
    private int alturaInicial;

    private final int X_PADDING;
    private final int Y_PADDING;

    /**
     * Declared for graphic purposes.
     */
    private int alturaMaxima = 150;
    private int puntoEnXMaximo = 250;
    private boolean mostrarTrayectorias;

    private final ArrayList<ArrayList<Point>> proyectiles;
    private final ArrayList<Color> proyectilesColores;

    private double escalaEjeX;
    private double escalaEjeY;

    public ParabolicShotPanel(int width, int height, int initialAngle, int alturaInicial) {
        setName("Parabolic shot panel");
        setSize(new Dimension(width, height));

        // Default values:
        setAnguloDisparo(initialAngle);
        setAlturaInicial(alturaInicial);
        setBackground(Color.white);
        X_PADDING = (int) (getWidth() * 0.03);
        Y_PADDING = (int) (getHeight() * 0.05);

        mostrarTrayectorias = true;

        proyectiles = new ArrayList<>();
        proyectilesColores = new ArrayList<>();
    }

    /**
     * Paints the panel in the graphics object given as a parameter.
     *
     * @param g Graphic object where the panel will be painted.
     */
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawAxis(g);

        int diameter = 4;
        int radius = diameter / 2;
        double yOffset = (double) alturaInicial / alturaMaxima;
        if (!mostrarTrayectorias) {
            diameter = 8;
            radius = diameter / 2;
        }
        int i = 0;
        for (ArrayList<Point> projectile : proyectiles) {
            g.setColor(proyectilesColores.get(i));
            if (mostrarTrayectorias) {
                for (Point point : projectile) {
                    int xPosition = (int) (point.x * escalaEjeX);
                    xPosition = xPosition + X_PADDING;
                    int yPosition = (int) (point.y * escalaEjeY);
                    yPosition = this.getHeight() - Y_PADDING - yPosition;
                    g.fillOval(xPosition - radius, (int) (yPosition - radius - yOffset),
                            diameter, diameter);
                }
            } else {
                if (projectile.size() > 1) {
                    int xPosition = (int) (projectile.get(projectile.size() - 1).x
                            * escalaEjeX);
                    xPosition = xPosition + X_PADDING;
                    int yPosition = (int) (projectile.get(projectile.size() - 1).y
                            * escalaEjeY);
                    yPosition = this.getHeight() - Y_PADDING - yPosition;
                    g.fillOval(xPosition - radius, (int) (yPosition - radius - yOffset),
                            diameter, diameter);
                }
            }
            ++i;
        }
    }

    private void drawAxis(Graphics g) {
        final int X_AXIS_LENGTH = this.getWidth() - X_PADDING - X_PADDING;
        final int Y_AXIS_LENGTH = this.getHeight() - Y_PADDING - Y_PADDING;

        final int MAJOR_SEPARATORS_LENGTH = 12;
        final int MINOR_SEPARATORS_LENGTH = 6;
        final int MINOR_MAJOR_PROPORTION = 4;

        // Eje Y
        final int Y_AXIS_MINOR_SPACING = 25;
        final int Y_AXIS_MINOR_INTERVALS = alturaMaxima / Y_AXIS_MINOR_SPACING;
        final int Y_AXIS_MINOR_PIXELS_SPACING = Y_AXIS_LENGTH
                / Y_AXIS_MINOR_INTERVALS;

        int i = 0;
        while (i < Y_AXIS_MINOR_INTERVALS + 1) {

            if (i % MINOR_MAJOR_PROPORTION == 0) {
                // Draw vertical numbers
                g.drawString("" + Y_AXIS_MINOR_SPACING * i,
                        X_PADDING - MAJOR_SEPARATORS_LENGTH - MAJOR_SEPARATORS_LENGTH,
                        this.getHeight() - Y_PADDING - (i * Y_AXIS_MINOR_PIXELS_SPACING)
                                - MINOR_SEPARATORS_LENGTH);
                // ???
                g.drawLine(X_PADDING - MAJOR_SEPARATORS_LENGTH,
                        this.getHeight() - Y_PADDING - (i * Y_AXIS_MINOR_PIXELS_SPACING),
                        X_PADDING,
                        this.getHeight() - Y_PADDING - (i * Y_AXIS_MINOR_PIXELS_SPACING));
            } else {
                // Vertical intermediate lines
                g.drawLine(X_PADDING - MINOR_SEPARATORS_LENGTH,
                        this.getHeight() - Y_PADDING - (i * Y_AXIS_MINOR_PIXELS_SPACING),
                        X_PADDING,
                        this.getHeight() - Y_PADDING - (i * Y_AXIS_MINOR_PIXELS_SPACING));
            }
            i++;
        }

        // Vertical Line
        g.drawLine(X_PADDING, this.getHeight() - Y_PADDING - (Y_AXIS_MINOR_INTERVALS * Y_AXIS_MINOR_PIXELS_SPACING),
                X_PADDING, this.getHeight() - Y_PADDING);

        // X AXIS
        final int X_AXIS_MINOR_SPACING = 25;
        final int X_AXIS_MINOR_INTERVALS = this.puntoEnXMaximo / X_AXIS_MINOR_SPACING;
        final int X_AXIS_MINOR_PIXELS_SPACING = X_AXIS_LENGTH
                / X_AXIS_MINOR_INTERVALS;

        i = 0;
        while (i < X_AXIS_MINOR_INTERVALS + 1) {
            if (i % MINOR_MAJOR_PROPORTION == 0) {
                g.drawString("" + X_AXIS_MINOR_SPACING * i,
                        X_PADDING + MINOR_SEPARATORS_LENGTH
                                + (i * X_AXIS_MINOR_PIXELS_SPACING),
                        this.getHeight() - Y_PADDING + MAJOR_SEPARATORS_LENGTH
                                + MINOR_SEPARATORS_LENGTH);
                g.drawLine(X_PADDING + (i * X_AXIS_MINOR_PIXELS_SPACING),
                        this.getHeight() - Y_PADDING,
                        X_PADDING + (i * X_AXIS_MINOR_PIXELS_SPACING),
                        this.getHeight() - Y_PADDING + MAJOR_SEPARATORS_LENGTH);
            } else {
                g.drawLine(X_PADDING + (i * X_AXIS_MINOR_PIXELS_SPACING),
                        this.getHeight() - Y_PADDING,
                        X_PADDING + (i * X_AXIS_MINOR_PIXELS_SPACING),
                        this.getHeight() - Y_PADDING + MINOR_SEPARATORS_LENGTH);
            }
            i++;
        }

        g.drawLine(X_PADDING, this.getHeight() - Y_PADDING,
                X_PADDING
                        + (int) (X_AXIS_MINOR_INTERVALS * X_AXIS_MINOR_PIXELS_SPACING),
                this.getHeight() - Y_PADDING);

        // Arrow
        final int ARROW_HYPOTENUSE = 20;
        final int OPPOSITE = (int) (Math.sin(Math.toRadians(anguloDisparo))
                * ARROW_HYPOTENUSE);
        final int ADJACENT = (int) (Math.cos(Math.toRadians(anguloDisparo))
                * ARROW_HYPOTENUSE);
        double offset = (double) alturaInicial / alturaMaxima;
        offset *= (Y_AXIS_MINOR_INTERVALS * Y_AXIS_MINOR_PIXELS_SPACING);
        g.drawLine(X_PADDING, (int) (getHeight() - Y_PADDING - offset),
                X_PADDING + ADJACENT,
                (int) (getHeight() - Y_PADDING - OPPOSITE - offset));

        escalaEjeX = (double) X_AXIS_LENGTH
                / (double) puntoEnXMaximo;
        escalaEjeY = (double) Y_AXIS_LENGTH / (double) alturaMaxima;
    }

    public void addProjectile() {
        proyectiles.add(new ArrayList<>());
        final int BOUND = 255;
        Random randomNumberGenerator = new Random();
        proyectilesColores.add(new Color(randomNumberGenerator.nextInt(BOUND),
                randomNumberGenerator.nextInt(BOUND),
                randomNumberGenerator.nextInt(BOUND)));
        repaint();
    }

    public void addPoint(Point newPoint) {
        proyectiles.get(proyectiles.size() - 1).add(newPoint);
        if (newPoint.x > puntoEnXMaximo) {
            puntoEnXMaximo = newPoint.x + 20;
        }
        if (newPoint.y > alturaMaxima) {
            alturaMaxima = newPoint.y + 20;
        }
        repaint();
    }

    // Getters and setters
    public int getShotAngle() {
        return anguloDisparo;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getAnguloDisparo() {
        return anguloDisparo;
    }

    public void setAnguloDisparo(int anguloDisparo) {
        this.anguloDisparo = anguloDisparo;
    }

    public int getDistanciaMaxima() {
        return distanciaMaxima;
    }

    public void setDistanciaMaxima(int distanciaMaxima) {
        this.distanciaMaxima = distanciaMaxima;
    }

    public int getAlturaInicial() {
        return alturaInicial;
    }

    public void setAlturaInicial(int alturaInicial) {
        this.alturaInicial = alturaInicial;
    }

    public int getalturaMaxima() {
        return alturaMaxima;
    }

    public void setalturaMaxima(int alturaMaxima) {
        this.alturaMaxima = alturaMaxima;
    }

    public boolean isMostrarTrayectorias() {
        return mostrarTrayectorias;
    }

    public void setMostrarTrayectorias(boolean mostrarTrayectorias) {
        this.mostrarTrayectorias = mostrarTrayectorias;
    }

    public ArrayList<ArrayList<Point>> getProyectiles() {
        return proyectiles;
    }

    public ArrayList<Color> getProyectilesColores() {
        return proyectilesColores;
    }

    /**
     * Changes the visibility of the trajectories.
     */
    public void changeShowTrajectories() {
        mostrarTrayectorias = !mostrarTrayectorias;
        repaint();
    }

}
