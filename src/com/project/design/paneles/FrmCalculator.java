package com.project.design.paneles;

import com.project.logic.LanzamientoP;
import com.project.logic.Proyectil;
import static com.project.utilities.Validate.*;
import javax.swing.JOptionPane;

public class FrmCalculator extends javax.swing.JInternalFrame {

    private final Proyectil proyectil;
    private final LanzamientoP lanzamiento;

    public FrmCalculator() {
        initComponents();
        proyectil = new Proyectil();
        lanzamiento = new LanzamientoP();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlHeader = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        pnlVars = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbVar1 = new javax.swing.JComboBox<>();
        cmbVar2 = new javax.swing.JComboBox<>();
        txtVar2 = new javax.swing.JTextField();
        btnCalcular = new javax.swing.JButton();
        txtVar1 = new javax.swing.JTextField();
        pnlLeft = new javax.swing.JPanel();
        lbl2 = new javax.swing.JLabel();
        txtVelocity = new javax.swing.JTextField();
        lbl3 = new javax.swing.JLabel();
        txtAngle = new javax.swing.JTextField();
        lbl4 = new javax.swing.JLabel();
        txtAlcance = new javax.swing.JTextField();
        lbl5 = new javax.swing.JLabel();
        txtTime = new javax.swing.JTextField();
        txtEquation = new javax.swing.JTextField();
        lbl6 = new javax.swing.JLabel();
        txtVelocityY = new javax.swing.JTextField();
        lbl8 = new javax.swing.JLabel();
        lbl9 = new javax.swing.JLabel();
        txtHeight = new javax.swing.JTextField();
        txtVelocityX = new javax.swing.JTextField();
        pnlRight = new javax.swing.JPanel();
        txtVarItem = new javax.swing.JTextField();
        cmbVarItem = new javax.swing.JComboBox<>();
        btnCalcItems = new javax.swing.JButton();
        lbl7 = new javax.swing.JLabel();
        txtTime1 = new javax.swing.JTextField();
        txtAlcance1 = new javax.swing.JTextField();
        lbl13 = new javax.swing.JLabel();
        txtVelocityX1 = new javax.swing.JTextField();
        txtHeight1 = new javax.swing.JTextField();
        lbl10 = new javax.swing.JLabel();
        lbl12 = new javax.swing.JLabel();
        lbl11 = new javax.swing.JLabel();
        txtVelocityY1 = new javax.swing.JTextField();
        txtVelocity1 = new javax.swing.JTextField();
        lbl14 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(232, 228, 228));
        setBorder(null);
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setForeground(new java.awt.Color(255, 255, 255));

        pnlHeader.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setFont(new java.awt.Font("Roboto", 1, 30)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 102));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("CALCULADORA");

        javax.swing.GroupLayout pnlHeaderLayout = new javax.swing.GroupLayout(pnlHeader);
        pnlHeader.setLayout(pnlHeaderLayout);
        pnlHeaderLayout.setHorizontalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlHeaderLayout.setVerticalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        pnlVars.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        jLabel1.setText("Datos conocidos");

        cmbVar1.setBackground(new java.awt.Color(255, 255, 255));
        cmbVar1.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        cmbVar1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccionar--", "Velocidad", "Ángulo", "Velocidad en x" }));
        cmbVar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbVar1ActionPerformed(evt);
            }
        });

        cmbVar2.setBackground(new java.awt.Color(255, 255, 255));
        cmbVar2.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        cmbVar2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccionar--", "Velocidad", "Ángulo", "Tiempo de vuelo", "Altura máxima", "Alcance máximo", "Velocidad en y" }));
        cmbVar2.setEnabled(false);
        cmbVar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbVar2ActionPerformed(evt);
            }
        });

        txtVar2.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        txtVar2.setEnabled(false);

        btnCalcular.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        btnCalcular.setText("Calcular");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });

        txtVar1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        txtVar1.setEnabled(false);

        javax.swing.GroupLayout pnlVarsLayout = new javax.swing.GroupLayout(pnlVars);
        pnlVars.setLayout(pnlVarsLayout);
        pnlVarsLayout.setHorizontalGroup(
            pnlVarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVarsLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(pnlVarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCalcular, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlVarsLayout.createSequentialGroup()
                        .addComponent(cmbVar1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtVar1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addComponent(cmbVar2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtVar2, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addContainerGap(64, Short.MAX_VALUE))
        );
        pnlVarsLayout.setVerticalGroup(
            pnlVarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVarsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlVarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbVar1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbVar2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVar2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVar1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCalcular, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlLeft.setBackground(new java.awt.Color(255, 255, 255));

        lbl2.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl2.setText("Velocidad");

        txtVelocity.setEditable(false);
        txtVelocity.setBackground(new java.awt.Color(255, 255, 255));
        txtVelocity.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        txtVelocity.setDisabledTextColor(new java.awt.Color(255, 255, 255));

        lbl3.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl3.setText("Ángulo");

        txtAngle.setEditable(false);
        txtAngle.setBackground(new java.awt.Color(255, 255, 255));
        txtAngle.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        lbl4.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl4.setText("Alcance");

        txtAlcance.setEditable(false);
        txtAlcance.setBackground(new java.awt.Color(255, 255, 255));
        txtAlcance.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        lbl5.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl5.setText("Tiempo");

        txtTime.setEditable(false);
        txtTime.setBackground(new java.awt.Color(255, 255, 255));
        txtTime.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        txtEquation.setEditable(false);
        txtEquation.setBackground(new java.awt.Color(255, 255, 255));
        txtEquation.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        lbl6.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl6.setText("Velocidad y");

        txtVelocityY.setEditable(false);
        txtVelocityY.setBackground(new java.awt.Color(255, 255, 255));
        txtVelocityY.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        lbl8.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl8.setText("Velocidad x");

        lbl9.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl9.setText("Altura");

        txtHeight.setEditable(false);
        txtHeight.setBackground(new java.awt.Color(255, 255, 255));
        txtHeight.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        txtVelocityX.setEditable(false);
        txtVelocityX.setBackground(new java.awt.Color(255, 255, 255));
        txtVelocityX.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtEquation, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlLeftLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lbl6)
                                .addGap(42, 42, 42)
                                .addComponent(txtVelocityY, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lbl8)
                                .addGap(42, 42, 42)
                                .addComponent(txtVelocityX, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lbl9)
                                .addGap(42, 42, 42)
                                .addComponent(txtHeight, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lbl4)
                                .addGap(42, 42, 42)
                                .addComponent(txtAlcance, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lbl5)
                                .addGap(42, 42, 42)
                                .addComponent(txtTime, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lbl3)
                                .addGap(42, 42, 42)
                                .addComponent(txtAngle, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lbl2)
                                .addGap(42, 42, 42)
                                .addComponent(txtVelocity, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl2)
                    .addComponent(txtVelocity, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl3)
                    .addComponent(txtAngle, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl5)
                    .addComponent(txtTime, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl4)
                    .addComponent(txtAlcance, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl9)
                    .addComponent(txtHeight, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl8)
                    .addComponent(txtVelocityX, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl6)
                    .addComponent(txtVelocityY, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(txtEquation, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        pnlRight.setBackground(new java.awt.Color(255, 255, 255));

        txtVarItem.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        txtVarItem.setEnabled(false);

        cmbVarItem.setBackground(new java.awt.Color(255, 255, 255));
        cmbVarItem.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        cmbVarItem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tiempo", "Altura de subida", "Altura de bajada", "Distancia", "Velocidad", "Velocidad en y", " " }));
        cmbVarItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbVarItemActionPerformed(evt);
            }
        });

        btnCalcItems.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        btnCalcItems.setText("Calcular");
        btnCalcItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcItemsActionPerformed(evt);
            }
        });

        lbl7.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl7.setText("Velocidad");

        txtTime1.setEditable(false);
        txtTime1.setBackground(new java.awt.Color(255, 255, 255));
        txtTime1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        txtAlcance1.setEditable(false);
        txtAlcance1.setBackground(new java.awt.Color(255, 255, 255));
        txtAlcance1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        lbl13.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl13.setText("Velocidad x");

        txtVelocityX1.setEditable(false);
        txtVelocityX1.setBackground(new java.awt.Color(255, 255, 255));
        txtVelocityX1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        txtHeight1.setEditable(false);
        txtHeight1.setBackground(new java.awt.Color(255, 255, 255));
        txtHeight1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        lbl10.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl10.setText("Tiempo");

        lbl12.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl12.setText("Altura");

        lbl11.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl11.setText("Alcance");

        txtVelocityY1.setEditable(false);
        txtVelocityY1.setBackground(new java.awt.Color(255, 255, 255));
        txtVelocityY1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N

        txtVelocity1.setEditable(false);
        txtVelocity1.setBackground(new java.awt.Color(255, 255, 255));
        txtVelocity1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        txtVelocity1.setDisabledTextColor(new java.awt.Color(255, 255, 255));

        lbl14.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lbl14.setText("Velocidad y");

        javax.swing.GroupLayout pnlRightLayout = new javax.swing.GroupLayout(pnlRight);
        pnlRight.setLayout(pnlRightLayout);
        pnlRightLayout.setHorizontalGroup(
            pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRightLayout.createSequentialGroup()
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRightLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(cmbVarItem, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtVarItem, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(btnCalcItems, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlRightLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlRightLayout.createSequentialGroup()
                                .addComponent(lbl14)
                                .addGap(42, 42, 42)
                                .addComponent(txtVelocityY1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlRightLayout.createSequentialGroup()
                                .addComponent(lbl13)
                                .addGap(42, 42, 42)
                                .addComponent(txtVelocityX1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlRightLayout.createSequentialGroup()
                                .addComponent(lbl12)
                                .addGap(42, 42, 42)
                                .addComponent(txtHeight1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlRightLayout.createSequentialGroup()
                                .addComponent(lbl11)
                                .addGap(42, 42, 42)
                                .addComponent(txtAlcance1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlRightLayout.createSequentialGroup()
                                .addComponent(lbl10)
                                .addGap(42, 42, 42)
                                .addComponent(txtTime1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlRightLayout.createSequentialGroup()
                                .addComponent(lbl7)
                                .addGap(42, 42, 42)
                                .addComponent(txtVelocity1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlRightLayout.setVerticalGroup(
            pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRightLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbVarItem, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVarItem, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCalcItems, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl7)
                    .addComponent(txtVelocity1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl10)
                    .addComponent(txtTime1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl11)
                    .addComponent(txtAlcance1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl12)
                    .addComponent(txtHeight1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl13)
                    .addComponent(txtVelocityX1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl14)
                    .addComponent(txtVelocityY1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlVars, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlRight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(pnlHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlVars, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlRight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlLeft, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
        if (validatePrinciaplSelect()) {
            double velocity = 0, angle = 0;
            int index1 = cmbVar1.getSelectedIndex();
            int index2 = cmbVar2.getSelectedIndex();

            double var1 = Double.parseDouble(txtVar1.getText());
            double var2 = Double.parseDouble(txtVar2.getText());

            switch (index1) {
                case 1 -> {
                    velocity = var1;
                    switch (index2) {
                        case 1 -> {
                            angle = var2;
                        }
                        case 2 -> {
                            angle = proyectil.getAngleVT(velocity, var2);
                        }
                        case 3 -> {
                            angle = proyectil.getAngleVH(velocity, var2);
                        }
                        case 4 -> {
                            angle = proyectil.getAngleVA(velocity, var2);
                        }
                    }
                }
                case 2 -> {
                    angle = var1;
                    switch (index2) {
                        case 1 -> {
                            velocity = var2;
                        }
                        case 2 -> {
                            velocity = proyectil.getVelocityTD(var2, angle);
                        }
                        case 3 -> {
                            velocity = proyectil.getVelocityHD(var2, angle);
                        }
                        case 4 -> {
                            velocity = proyectil.getVelocityAD(var2, angle);
                        }
                    }
                }
                case 3 -> {
                    angle = proyectil.getAngleVXY(var1, var2);
                    velocity = proyectil.getVelocityVXY(var1, var2);
                }
            }

            loadData(velocity, angle);
        } else {
            JOptionPane.showMessageDialog(null, "Debe ingresar una entrada válida");
        }
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void cmbVar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbVar1ActionPerformed
        int index = cmbVar1.getSelectedIndex();

        switch (index) {
            case 0 -> {
                cmbVar2.setEnabled(false);
                txtVar1.setEnabled(false);
            }
            case 1 -> {
                cmbVar2.setEnabled(true);
                loadCombo("Ángulo", true);
                txtVar1.setEnabled(true);
            }
            case 2 -> {
                cmbVar2.setEnabled(true);
                loadCombo("Velocidad", true);
                txtVar1.setEnabled(true);
            }
            case 3 -> {
                cmbVar2.setEnabled(true);
                loadCombo("Velocidad en y", false);
                txtVar1.setEnabled(true);
            }
        }

    }//GEN-LAST:event_cmbVar1ActionPerformed

    private void cmbVar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbVar2ActionPerformed
        int index = cmbVar2.getSelectedIndex();

        switch (index) {
            case 0 ->
                txtVar2.setEnabled(false);
            case 1,2,3,4 ->
                txtVar2.setEnabled(true);
        }
    }//GEN-LAST:event_cmbVar2ActionPerformed

    private void cmbVarItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbVarItemActionPerformed
        int index = cmbVarItem.getSelectedIndex();

        switch (index) {
            case 0, 1,2,3,4,5 ->
                txtVarItem.setEnabled(true);
        }
    }//GEN-LAST:event_cmbVarItemActionPerformed

    private void btnCalcItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcItemsActionPerformed
        if (validateSecondarySelect()) {
            double time = 0;
            double var = Double.parseDouble(txtVarItem.getText());
            int index = cmbVarItem.getSelectedIndex();

            time = switch (index) {
                case 0 ->
                    Double.parseDouble(txtVarItem.getText());
                case 1 ->
                    lanzamiento.getTimeH(var, true);
                case 2 ->
                    lanzamiento.getTimeH(var, false);
                case 3 ->
                    lanzamiento.getTimeA(var);
                case 4 ->
                    lanzamiento.getTimeV(var);
                case 5 ->
                    lanzamiento.getTimeVy(var);
                default ->
                    time;
            };

            if (time != -1) {
                txtAlcance1.setText(lanzamiento.getAlcance(time) + "");
                txtHeight1.setText(lanzamiento.getAltura(time) + "");
                txtVelocityX1.setText(lanzamiento.getVx() + "");
                txtVelocityY1.setText(lanzamiento.getVy(time) + "");
                txtVelocity1.setText(lanzamiento.getV(time) + "");
                txtTime1.setText(round(3, time) + "");
            } else {
                JOptionPane.showMessageDialog(null,
                        "Esta variable jamás alcanza este valor");
            }
        } else {
            JOptionPane.showMessageDialog(null,
                    "Entrada no válida");
        }
    }//GEN-LAST:event_btnCalcItemsActionPerformed

    private void loadCombo(String var, boolean all) {
        cmbVar2.removeAllItems();
        cmbVar2.addItem("--Seleccionar--");
        cmbVar2.addItem(var);
        if (!all) {
            return;
        }
        cmbVar2.addItem("Tiempo de vuelo");
        cmbVar2.addItem("Altura máxima");
        cmbVar2.addItem("Alcance máximo");
    }

    private void loadData(double velocity, double angle) {
        lanzamiento.setInitialVelocity(velocity);
        lanzamiento.setAngle(angle);

        txtVelocity.setText(velocity + "");
        txtAngle.setText(angle + "");
        txtAlcance.setText(lanzamiento.getAlcanceM() + "");
        txtHeight.setText(lanzamiento.getAlturaM() + "");
        txtTime.setText(lanzamiento.getTimef() + "");
        txtVelocityX.setText(lanzamiento.getVoxI() + "");
        txtVelocityY.setText(lanzamiento.getVoyI() + "");
        txtEquation.setText(lanzamiento.getEcuacion());
    }

    private boolean validatePrinciaplSelect() {
        int index1 = cmbVar1.getSelectedIndex();
        int index2 = cmbVar2.getSelectedIndex();

        if (index1 != 0 && index2 != 0) {
            String var1 = txtVar1.getText();
            String var2 = txtVar2.getText();
            if (isValidDouble(var1) && isValidDouble(var2)) {
                return true;
            }
        }
        return false;
    }

    private boolean validateSecondarySelect() {
        return isValidDouble(txtVarItem.getText());
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcItems;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JComboBox<String> cmbVar1;
    private javax.swing.JComboBox<String> cmbVar2;
    private javax.swing.JComboBox<String> cmbVarItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel lbl10;
    private javax.swing.JLabel lbl11;
    private javax.swing.JLabel lbl12;
    private javax.swing.JLabel lbl13;
    private javax.swing.JLabel lbl14;
    private javax.swing.JLabel lbl2;
    private javax.swing.JLabel lbl3;
    private javax.swing.JLabel lbl4;
    private javax.swing.JLabel lbl5;
    private javax.swing.JLabel lbl6;
    private javax.swing.JLabel lbl7;
    private javax.swing.JLabel lbl8;
    private javax.swing.JLabel lbl9;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private javax.swing.JPanel pnlVars;
    private javax.swing.JTextField txtAlcance;
    private javax.swing.JTextField txtAlcance1;
    private javax.swing.JTextField txtAngle;
    private javax.swing.JTextField txtEquation;
    private javax.swing.JTextField txtHeight;
    private javax.swing.JTextField txtHeight1;
    private javax.swing.JTextField txtTime;
    private javax.swing.JTextField txtTime1;
    private javax.swing.JTextField txtVar1;
    private javax.swing.JTextField txtVar2;
    private javax.swing.JTextField txtVarItem;
    private javax.swing.JTextField txtVelocity;
    private javax.swing.JTextField txtVelocity1;
    private javax.swing.JTextField txtVelocityX;
    private javax.swing.JTextField txtVelocityX1;
    private javax.swing.JTextField txtVelocityY;
    private javax.swing.JTextField txtVelocityY1;
    // End of variables declaration//GEN-END:variables
}
